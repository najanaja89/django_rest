from rest_framework import serializers
from .models import TodoItem

class TodoItemSerializer(serializers.ModelSerializer):

    class Meta:
        model = TodoItem
        fields = 'id', 'text', 'done'

    done  = serializers.BooleanField(required=False, default=False)
    # id = serializers.IntegerField()
    # text = serializers.CharField()

