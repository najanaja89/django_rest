from django.urls import path
from . import views

app_name = 'todoapp'

urlpatterns = [
    path('', views.index_view),
    path('api/todos', views.TodoItemList.as_view()),
    path('api/todo/<int:pk>', views.TodoDetailView.as_view()),
]
