from django.http import HttpResponse
from django.shortcuts import render, get_object_or_404
from .models import TodoItem

# Create your views here.
from rest_framework.views import APIView
from rest_framework.response import Response
from .serializers import TodoItemSerializer
from rest_framework.status import HTTP_418_IM_A_TEAPOT


def index_view(request):
    return HttpResponse('OK')


class TodoItemList(APIView):
    def get(self, request):
        items = TodoItem.objects.all()
        serializer = TodoItemSerializer(items, many=True)

        return Response(serializer.data)

    def post(self, request):
        serializer = TodoItemSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=HTTP_418_IM_A_TEAPOT)

    def delete(self, request):
        pass


class TodoDetailView(APIView):
    def get(self, request, pk):
        todo = get_object_or_404(TodoItem, pk=pk)
        serializer = TodoItemSerializer(todo)
        return Response(serializer.data)

    def delete(self, request, pk):
        todo = get_object_or_404(TodoItem, pk=pk)
        serializer = TodoItemSerializer(todo)
        data = serializer.data
        todo.delete()
        return Response(data)
